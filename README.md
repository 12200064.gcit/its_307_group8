# its_307_GROUP8


<h1 align="center">Rainfall Prediction Using Machine Learning Algoritherm </h1>



A web program called Rainfall Prediction is used to forecast monthly rainfall in specific Bhutanese regions. Rainfall prediction uses variables including the maximum temperature, minimum temperature, relative humidity, and wind speed to forecast the amount of rainfall that will fall each month in millimeters in a user-selected location.

<h3 align="center">Rainfall Prediction?</h3>

1. The ability to anticipate rainfall with accuracy and precision is currently missing, which might be helpful in a variety of industries like agriculture, water conservation, and flood prediction. 
2. To provide output estimates that are accurate and dependable, Rainfall prediction bases its analysis on prior discoveries and similarities. 
3. To address issues with aviation and road transportation so that travelers are correctly and effectively informed 

<h3 align="center">How does it work?</h3>

The KNN algoritherm is trained to predict monthly rainfall using following parameters:

- Maximum Temperature
- Minimum Temperature 
- Relative Humidity
- Windspeed
- Month 
- Year
- Locations
- Rainfall 


*Visit our Web-Application [RainfallPrediction](https://12200065-rainfall-app-scvkvs.streamlit.app/?fbclid=IwAR1OcI7TbdcS5r90A8a7oLroJ2AblVGwZY3s92ZgXVFk3XwrUbgbZecP5Gs)*






<h1 align="center">Screenshots of RainDate Web-Application</h1>
<h1 align="center"><img src='https://gitlab.com/12200064.gcit/its_307_group8/-/blob/main/Home.png'></h1>
<h1 align="center"><img src='https://gitlab.com/12200064.gcit/its_307_group8/-/blob/main/result.png'></h1>
<h1 align="center"><img src='https://gitlab.com/12200064.gcit/its_307_group8/-/blob/main/Explore.png'></h1>
<h1 align="center"><img src='https://gitlab.com/12200064.gcit/its_307_group8/-/blob/main/Predict.png'></h1>
<h1 align="center"><img src='https://gitlab.com/12200064.gcit/its_307_group8/-/blob/main/Team.png'></h1>




